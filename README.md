yii2-topvisor
=============
yii2-topvisor

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sitis/yii2-topvisor "*"
```

or add

```
"sitis/yii2-topvisor": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \sitis\topvisor\AutoloadExample::widget(); ?>```